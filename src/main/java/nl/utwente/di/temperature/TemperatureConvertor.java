package nl.utwente.di.temperature;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class TemperatureConvertor extends HttpServlet {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public void init() throws ServletException {}

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius > Farenheit";

        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n");

        String param = request.getParameter("degrees");
        double degrees = 0;
        if (param != null) {
            try {
                degrees = Double.parseDouble(param);
                out.println("<P>answer: " + (Math.round(((degrees * 1.8) + 32) * 100.0) / 100.0) + "</P> ");
            } catch (NumberFormatException e) {
                out.println("Please supply a valid number");
            }
        } else {
            out.println("Please supply a number of degrees (GET parameter 'degrees')");
        }

        out.println("</BODY></HTML>");
    }


}